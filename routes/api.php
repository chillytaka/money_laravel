<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', function() {
  return response('test api', 200)->header('Content-Type', 'application/json');
});

// Auth Route
Route::post('/signup', 'AuthController@signup');
Route::post('/login', 'AuthController@signin')->middleware('throttle:3,10');

Route::group(['middleware' => ['auth:api']], function () {

  // app account control route
  Route::post('/edit_pass', 'AuthController@editPassword');
  Route::post('/edit_name', 'AuthController@editName');
  Route::post('/del_account', 'AuthController@delAccount');

  // bank account control route
  Route::post('/add_bank_account', 'AccountController@addAccount');
  Route::post('/del_bank_account', 'AccountController@delAccount');
  Route::post('/edit_bank_account', 'AccountController@updateAccount');
  Route::post('/list_bank_account', 'AccountController@listAccount');
  Route::post('/get_bank_account', 'AccountController@getAccount');

  //financial activity route
  Route::post('/add_bank_activity', 'ActivityController@addActivity');
  Route::post('/del_bank_activity', 'ActivityController@delActivity');
  Route::post('/list_bank_activity', 'ActivityController@listActivity');
  Route::post('/list_all_bank_activity', 'ActivityController@listAllActivity');
  Route::post('/get_bank_activity', 'ActivityController@getActivity');

});
