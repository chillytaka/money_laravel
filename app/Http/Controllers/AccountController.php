<?php

namespace App\Http\Controllers;

use App\Account;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * Action error message
     * 
     * @return Response
     */
    protected function actError($errCode = null) {
        return response()->json([
            "status" => 400,
            "message" => "Action Failed for Unknown Reasons (" . $errCode . ")"
        ], 400);
    }

    /**
     * Add a new bank account
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return Response
     */
    public function addAccount(Request $request)
    {
        $user = $request->user();

        $store = Account::create([
            'user_id' => $user->id,
            'account' => $request['account'],
            'account_amount' => $request['account_amount'],
            'account_details' => $request['account_details'],
        ]);

        if (isset($store)) {
            return response()->json([
                "status" => 200,
                'message' => "Account Added Successfully"
            ], 200 );
        }

        return $this->actError(100);
    }

    /**
     * Delete a new bank account
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return Response
     */
    public function delAccount(Request $request)
    {
        $user = $request->user();

        $result = Account::where( 'user_id', $user->id)
            ->where('id', $request['account_id'])
            ->delete();

        if ($result) {
            return response()->json([
                "status" => 200,
                'message' => "Account Deleted Successfully"
            ], 200 );
        }

        return $this->actError(102);
    }

    /**
     * List all account
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return Response
     */
    public function listAccount(Request $request) {
        $user = $request->user();

        $store = Account::where('user_id', $user->id)
            ->get();

        if ($store) {
            return response()->json([
                "status" => 200,
                "data" => $store
            ], 200);
        }

        return $this->actError(104);
    }

    /**
     * Update a new bank account
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return Response
     */
    public function updateAccount(Request $request)
    {
        $user = $request->user();

        $store = Account::where( 'user_id', $user->id)
            ->where('id', $request['account_id'])
            ->update([
            'user_id' => $user->id,
            'account' => $request['account'],
            'account_amount' => $request['account_amount'],
            'account_details' => $request['account_details'],
        ]);

        if ($store) {
            return response()->json([
                "status" => 200,
                'message' => "Account Updated Successfully"
            ], 200 );
        }

        return $this->actError(103);
    }

    /**
     * Get One Account
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return  Response
     */
    public function getAccount(Request $request) {
        $user = $request->user();

        $result = Account::where('user_id', $user->id)
            ->where("id", $request['account_id'])
            ->first();

        if (isset($result)) {
            return response()->json([
                "status" => 200,
                "data" => $result
            ], 200);
        }

        return $this->actError(104);
    }

}
