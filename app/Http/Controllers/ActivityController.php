<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Account;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    /**
     * Action error message
     * 
     * @return Response
     */
    protected function actError($errCode = null, $msgError = null) {
        if (!isset($msgError)) {

            return response()->json([
                "status" => 400,
                "message" => "Action Failed for Unknown Reason (" . $errCode . ")"
            ], 400);

        } else {

            return response()->json([
                "status" => 400,
                "message" => $msgError . " (" . $errCode . ")"
            ], 400);

        }
    }

    /**
     * checking the owner of the account
     * 
     * @param $user, $account_id
     * 
     * @return boolean
     */
    protected function checkOwner($user, $account_id) {
        
        // check if the requested account is really owned by the user
        $ret = Account::where('user_id', $user->id)
            ->where('id', $account_id)
            ->get();

        return ($ret->first());
    }

    /**
     * return success message
     * 
     * @param String $action,
     * 
     * @return Response
     */
    protected function successMsg($action) {
        return response()->json([
            "status" => 200,
            "message" => "Activity ". $action . " Successfully"
        ], 200 );
    }

    /**
     * Add a new activity
     * 
     * @param \Illuminate\Http\Request
     * 
     * @return response->json()
     */
    public function addActivity(Request $request) {
        $user = $request->user();
        
        $ret = $this->checkOwner($user, $request['account_id']);

        if ($ret) {

            $store = Activity::create([
                'account_id' => $request['account_id'],
                'activity_type' => $request['activity_type'],
                'activity_details' => $request['activity_details'],
                'activity_amount' => $request['activity_amount'],
                'ip_source' => $_SERVER["REMOTE_ADDR"]
            ]);

            if ($store) {
                return $this->successMsg("Added");
            }

        }
        
        else {
            return $this->actError(301, "Authentication Failed");
        }
        
        return $this->actError(201);

    }

    /**
     * List activity filtered by account and date (optional) and type ( optional )
     * 
     * @param \Illuminate\Http|Request
     * 
     * @return Response
     */
    public function listActivity(Request $request) {
        $user = $request->user();

        $ret = $this->checkOwner($user, $request['account_id']);

        if ($ret) {
            
            $store = Activity::where('account_id', $request['account_id']);

            if (isset($request['from_date']) && isset($request['to_date'])) {
                $store = $store->where('created_at', '>=', $request['from_date'])
                    ->where('created_at', '<=', $request['to_date']);
            }

            if (isset($request['activity_type'])) {
                $store = $store->where('activity_type', $request['activity_type']);
            }

            $store = $store->get();

            if($store) {
                return response()->json([
                    "status" => 200,
                    "data" => $store
                ], 200);
            }

        } else {
            return $this->actError(303, "Authentication Failed");
        }

        return $this->actError(203);
    }

    /**
     * List activity filtered by date (optional) and type ( optional )
     * 
     * @param \Illuminate\Http|Request
     * 
     * @return Response
     */
    public function listAllActivity(Request $request) {
        $user = $request->user();
        
        $listAccounts = Account::where("user_id", $user->id)->get();

        $finalResult = collect(new Activity); //finalVariable for storing all activity within one account

        foreach ($listAccounts as $account) {
            $tempResult = Activity::where('account_id', $account->id);
                
            if (isset($request['from_date']) && isset($request['to_date'])) {
                $tempResult= $tempResult->where('created_at', '>=', $request['from_date'])
                    ->where('created_at', '<=', $request['to_date']);
            }

            if (isset($request['activity_type'])) {
                $tempResult = $tempResult->where('activity_type', $request['activity_type']);
            }

            $tempResult= $tempResult->get();

            $finalResult = $finalResult->merge($tempResult);
        }

        return response()->json($finalResult, 200 );

    }


    /**
     * Delete an existing activity
     * 
     * @param \Illuminate\Http\Request
     * 
     * @return response->json()
     */
    public function delActivity(Request $request) {

        $user = $request->user();
        
        $ret = $this->checkOwner($user, $request['account_id']);

        if ($ret) {

            $store = Activity::where('account_id', $request['account_id'])
            ->where('id', $request['activity_id'])
            ->delete();

            if ($store) {
                return $this->successMsg('Deleted');
            }

        } else {
            return $this->actError(302, "Authentication Failed");
        }

        return $this->actError(202);
    }

    /**
     * Get One Activity
     * 
     * @param \Illuminate\Http\Request
     * 
     * @return Response
     */
    public function getActivity(Request $request) {
        $user = $request->user();

        $return = Activity::where('id', $request['activity_id'])->get();

        if ($return->first()) {
            $userCheck = $this->checkOwner($user, $return->first()->account_id);

            if ($userCheck) {
                return response()->json([
                    "status" => 200,
                    "data" => $return
                ], 200);
            } else {
                return $this->actError(303, "Authentication Failed");
            }
        }

        return $this->actError(203);
    }
}
