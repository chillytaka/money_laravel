<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Create new user
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function signup(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'numeric', 'min:10'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
 
        if ($validation->fails()) {
            return response()->json([
                "status" => 400, 
                "message" => $validation->errors()], 
                400);
        }

        else {
            $user = User::create([
                'name' => $request['name'],
                'email' => $request['email'],
                'phone' => $request['phone'],
                'password' => Hash::make($request['password']),
                'api_token' => Str::random(60),
            ]);

            return response()->json(["status" => 200, "data" => $user], 200);
        }

    }
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function signin(Request $request)
    {
        //validate form
        $validation = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string', 'min:8'],
        ]);

        $credentials = $request->only('email', 'password');
        if (Auth::once($credentials)) {

            $user = Auth::user();
            return response()->json([
                "status" => 200,
                "data" => $user], 200);

        }

        return $this->authError();
    }

    /**
     * Handle edit password request
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return Response
     */
    public function editPassword(Request $request)
    {
        //validate form
        $validation = Validator::make($request->all(), [
            'old_password' => ['required', 'string', 'min:8'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $user = $request->user();
        if (Hash::check($request->old_password, $user->password)) {
            $result = User::where('id', $user->id)->update([
                'password' => Hash::make($request->password),
            ]);
        }

        if (isset($result)) {
            return response()->json([
                "status" => 200,
                "message" => "Password changed succesfully"
            ], 200);
        } 

        return $this->authError();
    }

    /**
     * Handle edit name request
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return Response
     */
    public function editName(Request $request)
    {
        //validate form
        $validation = Validator::make($request->all(), [
            'name' => ['required', 'string'],
        ]);

        $user = $request->user();
        $result = User::where('id', $user->id)->update([
            'name' => $request->name,
        ]);

        if ($result) {
            return response()->json([
                "status" => 200,
                "message" => "Name changed succesfully"
            ], 200);
        }

        return $this->authError();
    }

    /**
     * Handle edit email request
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return Response
     */
    public function editEmail(Request $request) {

        //validate form
        $validation = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email'],
        ]);

        $user = $request->user();
        $result = User::where('id', $user->id)->update([
            'email' => $request->email,
        ]);

        if ($result) {
            return response()->json([
                "status" => 200,
                "message" => "Email changed succesfully"
            ], 200);
        }

        return $this->authError();
    }

    /**
     * Handle Delete Account 
     * 
     * @param \Illuminate\Http\Request $request
     * 
     * @return Response
     */
    public function delAccount(Request $request)
    {
        //validate form
        $validation = Validator::make($request->all(), [
            "email" => ['required', 'string', 'email'],
            'password' => ['required', 'string', 'min:8' ],
        ]);

        $user = $request->user();
        if (Hash::check($request->password, $user->password)) {
            $result = User::where('id', $user->id)->where('email', $request->email)
            ->delete();
        }

        if ($result) {
            return response()->json([
                "status" => 200,
                "message" => "Account Deleted Succefully"
            ], 200);
        } 

        return $this->authError();
    }

    /**
     * Authentication error message
     * 
     * @return Response
     */
    protected function authError() {
        return response()->json([
            "status" => 401,
            "message" => "Authentication Error"
        ], 401);
    }

}
